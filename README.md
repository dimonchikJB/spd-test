
***Installation***

1. ```php artisan key:generate```

2. ```composer install```

3. ```php artisan migrate```

4. ```composer dump-autoload```

***How to run tests***

```php artisan test```