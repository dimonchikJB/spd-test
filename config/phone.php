<?php

return [

    'default' => '/^\d+$/',

    'locale' => [
        'en' => '/^\+380\d{9}$/i'
    ]

];
