<?php

return [

    'services' => [
        'paypal' => [
            'client_id' => env('PAYPAL_CLIENT_ID', ''),
            'secret' => env('PAYPAL_SECRET', ''),
            'currency' => env('PAYPAL_CURRENCY', 'USD'),
            'settings' => [
                'mode' => env('PAYPAL_MODE','sandbox'),
                'http.ConnectionTimeOut' => 30,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'ERROR'
            ]
        ],

        'stripe' => [
            'client_id' => env('STRIPE_KEY', ''),
            'secret' => env('STRIPE_SECRET', '')
        ]
    ]

];
