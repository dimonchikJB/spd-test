<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::prefix('products')->name('products.')->group(function () {
    Route::get('', 'ProductController@index')->name('index');
    Route::post('', 'ProductController@store')->name('store');
    Route::get('{product}', 'ProductController@show')->name('show');
    Route::delete('{product}', 'ProductController@destroy')->name('destroy');
    Route::put('{product}/cover-image', 'ProductCoverImageController@update')->name('cover-image.update');
    Route::get('{product}/comments', 'ProductCommentController@index')->name('comments.index');
    Route::post('{product}/comments', 'ProductCommentController@store')->name('comments.store');
});

Route::prefix('comments')->name('comments.')->group(function () {
    Route::put('{comment}/like', 'CommentLikeController@update')->name('like');
    Route::delete('{comment}/dislike', 'CommentLikeController@destroy')->name('dislike');
});

Route::group(['namespace' => 'Payments'], function () {
    Route::post('stripe', 'StripeController@store')->name('stripe-create-payment');
    Route::post('paypal', 'PayPalController@store')->name('paypal-create-payment');
    Route::get('paypal', 'PayPalController@index')->name('paypal-redirect');
});

Route::prefix('wishlists')->name('wishlists.')->group(function () {
    Route::get('', 'WishListController@index')->name('index');
    Route::post('', 'WishListController@store')->name('store');
});

Route::post('checkout', 'CheckoutController@store')->name('order.store');
