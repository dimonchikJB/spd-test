<?php

namespace Tests\Feature;

use App\Cart;
use App\Order;
use App\Product;
use Facades\Tests\Setup\RoleFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class ManageOrderTest
 * @package Tests\Feature
 */
class ManageOrderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authorized_user_can_create_an_order()
    {
        $this->withoutExceptionHandling();
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'create order']
        ])
            ->assignToUser($user)
            ->create();

        $user->carts()->create(['is_active' => true]);

        factory(Product::class,5)->create(['price' => 10.20])->each(function ($product) use ($user){
            $user->activeCart->items()->create(['product_id' => $product->id]);
        });

        $order = factory(Order::class)->raw(['status' => '', 'amount' => '', 'cart_id' => $user->activeCart->id]);

        $this->post(route('order.store'), $order)
            ->assertRedirect();
    }
}
