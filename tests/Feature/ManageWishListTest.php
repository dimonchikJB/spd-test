<?php

namespace Tests\Feature;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Facades\Tests\Setup\RoleFactory;

/**
 * Class ManageWishListTest
 * @package Tests\Feature
 */
class ManageWishListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */

    public function an_authorized_user_can_add_product_to_wishlist()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'create wishlist']
        ])
            ->assignToUser($user)
            ->create();

        $product = factory(Product::class)->create();

        $this->post(route('wishlists.store'), ['product_id' => $product->id])
            ->assertRedirect();

        $this->assertDatabaseHas('wishlists', ['product_id' => $product->id]);
    }

    /** @test */
    public function an_unauthorized_user_can_not_add_product_to_wishlist()
    {
        $product = factory(Product::class)->create();

        $this->post(route('wishlists.store'), ['product_id' => $product->id])
            ->assertForbidden();
    }
}
