<?php

namespace Tests\Feature;

use App\Comment;
use App\Like;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Facades\Tests\Setup\RoleFactory;
use Facades\Tests\Setup\ProductFactory;

/**
 * Class ManageCommentLikeTest
 * @package Tests\Feature
 */
class ManageCommentLikeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authorized_user_can_like_comment()
    {
        $this->withoutExceptionHandling();

        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'like comment']
        ])
            ->assignToUser($user)
            ->create();

        $product = ProductFactory::withComments(1)
            ->assignToUser($user)
            ->create();


        $this->put(route('comments.like', ['comment' => $product->comments->first()->id]), [
            'action' => Like::ACTION_LIKE
        ])
            ->assertRedirect();

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'subject_type' => Comment::class,
            'subject_id' => $product->comments->first()->id,
            'action' => Like::ACTION_LIKE
        ]);
    }

    /** @test */
    public function an_unauthorized_user_can_not_like_a_product()
    {
        $product = ProductFactory::withComments(1)
            ->create();

        $this->put(route('comments.like', ['comment' => $product->comments->first()->id]), [
            'action' => Like::ACTION_LIKE
        ])
            ->assertForbidden();
    }

    /** @test */
    public function an_authorized_user_can_delete_like()
    {
        $this->withoutExceptionHandling();

        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'like comment'],
            ['name' => 'dislike comment']
        ])
            ->assignToUser($user)
            ->create();

        $product = ProductFactory::withComments(1)
            ->assignToUser($user)
            ->create();

        $this->put(route('comments.like', ['comment' => $product->comments->first()->id]), [
            'action' => Like::ACTION_LIKE
        ]);

        $this->delete(route('comments.dislike', ['comment' => $product->comments->first()->id]))
            ->assertRedirect();

        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id,
            'subject_type' => Comment::class,
            'subject_id' => $product->comments->first()->id
        ]);
    }

    /** @test */
    public function an_unauthorized_user_can_not_dislike_a_product()
    {
        $product = ProductFactory::withComments(1)
            ->create();

        $this->delete(route('comments.dislike', ['comment' => $product->comments->first()->id]))
            ->assertForbidden();
    }
}
