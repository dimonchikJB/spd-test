<?php

namespace Tests\Feature;

use App\Product;
use App\Services\ProductService;
use Tests\TestCase;
use Facades\Tests\Setup\RoleFactory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class ManageProductCommentTest
 * @package Tests\Feature
 */
class ManageProductCommentTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /** @test */
    public function an_authorized_user_can_add_a_comment()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'create comment']
        ])
            ->assignToUser($user)
            ->create();

        $product = factory(Product::class)->create();
        $text = $this->faker->text;

        $this->post(route('products.comments.store', ['product' => $product->id]), [
            'product_id' => $product->id,
            'text' => $text
        ])
            ->assertRedirect();

        $this->assertDatabaseHas('comments', ['text' => $text]);
    }

    /** @test */
    public function an_unauthorized_user_can_not_add_a_comment()
    {
        $product = factory(Product::class)->create();

        $this->post(route('products.comments.store', ['product' => $product->id]), [
            'product_id' => $product->id,
            'text' => $this->faker->text
        ])
            ->assertForbidden();
    }

    /** @test */
    public function a_user_can_browse_comments()
    {
        $this->loginUser();

        $productService = app(ProductService::class);

        $product = factory(Product::class)->create();

        $productService->addComment(['product_id' => $product->id, 'text' => $this->faker->text]);

        $this->get(route('products.comments.index', ['product' => $product->id]))
            ->assertJson(['comments' => $product->comments->toArray()]);
    }
}
