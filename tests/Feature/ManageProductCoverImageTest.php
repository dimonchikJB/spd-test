<?php

namespace Tests\Feature;

use Facades\Tests\Setup\RoleFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Testing\File;
use Tests\TestCase;
use Facades\Tests\Setup\ProductFactory;

/**
 * Class ManageProductCoverImageTest
 * @package Tests\Feature
 */
class ManageProductCoverImageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authorized_user_can_update_cover_image()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'update product']
        ])
            ->assignToUser($user)
            ->create();

        $product = ProductFactory::create();

        $image = File::image('test-cover-image.png', 200, 350);

        $this->put(route('products.cover-image.update', ['product' => $product->id]), [
            'image' => $image
        ])
            ->assertRedirect();

        $this->assertFileExists(storage_path('app/' . $product->getImagePath() . '/' . $image->hashName()));

        @unlink(storage_path('app/' . $product->getImagePath() . '/' . $image->hashName()));
    }

    /** @test */
    public function an_unauthorized_user_can_not_update_cover_image()
    {
        $product = ProductFactory::create();

        $image = File::image('test-cover-image.png', 200, 350);

        $this->put(route('products.cover-image.update', ['product' => $product->id]), [
            'image' => $image
        ])
            ->assertForbidden();
    }

    /** @test */
    public function a_user_can_not_upload_big_image()
    {
        $product = ProductFactory::create();

        $image = File::image('test-cover-image.png', 600, 850);

        $this->put(route('products.cover-image.update', ['product' => $product->id]), [
            'image' => $image
        ])
            ->assertForbidden();
    }
}
