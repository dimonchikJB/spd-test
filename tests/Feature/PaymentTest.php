<?php

namespace Tests\Feature;

use App\Order;
use App\Product;
use Facades\Tests\Setup\RoleFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaymentTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function an_authorizes_user_can_pay_via_stripe()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'pay']
        ])
            ->assignToUser($user)
            ->create();

        $user->carts()->create(['is_active' => true]);

        factory(Product::class,5)->create(['price' => 10.20])->each(function ($product) use ($user){
            $user->activeCart->items()->create(['product_id' => $product->id]);
        });

        $order = factory(Order::class)->create(['status' => '', 'amount' => 1000, 'cart_id' => $user->activeCart->id]);

        $this->post('stripe', [
            'payment_code' => 'stripe',
            'payment_method' => 'pm_card_visa',
            'order_id' => $order->id
        ])
            ->assertOk()
        ->assertSessionMissing('payment_error');

    }

    /** @test */
    public function an_authorizes_user_can_pay_via_paypal()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'pay']
        ])
            ->assignToUser($user)
            ->create();

        $user->carts()->create(['is_active' => true]);

        factory(Product::class,5)->create(['price' => 10.20])->each(function ($product) use ($user){
            $user->activeCart->items()->create(['product_id' => $product->id]);
        });

        $order = factory(Order::class)->create(['status' => '', 'amount' => 1000, 'cart_id' => $user->activeCart->id]);

        $this->post('paypal', [
            'payment_code' => 'paypal',
            'payment_method' => 'paypal',
            'order_id' => $order->id
        ])
            ->assertOk();
    }
}
