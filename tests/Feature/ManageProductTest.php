<?php

namespace Tests\Feature;

use App\Product;
use Stripe\Stripe;
use Tests\TestCase;
use Facades\Tests\Setup\RoleFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class ManageProductTest
 * @package Tests\Feature
 */
class ManageProductTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authorized_user_can_create_a_product()
    {
        $user = $this->loginUser();

        $product = factory(Product::class)->raw();

        RoleFactory::withPermissions([
            ['name' => 'create product']
        ])
            ->assignToUser($user)
            ->create();

        $this->post(route('products.store'), $product)
            ->assertRedirect();

        $this->assertDatabaseHas('products', $product);
    }

    /** @test */
    public function an_unauthorized_user_can_not_create_a_product()
    {
        $product = factory(Product::class)->raw();

        $this->post(route('products.store'), $product)
            ->assertForbidden();
    }

    /** @test */
    public function an_authorized_user_can_delete_a_product()
    {
        $user = $this->loginUser();
        $product = factory(Product::class)->create();

        RoleFactory::withPermissions([
            ['name' => 'delete product']
        ])
            ->assignToUser($user)
            ->create();

        $this->delete(route('products.destroy', ['product' => $product->id]))
            ->assertRedirect();

        $this->assertDatabaseMissing('products', $product->toArray());
    }

    /** @test */
    public function an_unauthorized_user_can_not_delete_a_product()
    {
        $product = factory(Product::class)->create();
        $this->delete(route('products.destroy', ['product' => $product->id]))
            ->assertForbidden();
    }

    /** @test */
    public function a_product_requires_a_sku()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'create product']
        ])
            ->assignToUser($user)
            ->create();

        $product = factory(Product::class)->raw(['sku' => '']);

        $this->post(route('products.store'), $product)
            ->assertSessionHasErrors(['sku']);
    }

    /** @test */
    public function a_product_requires_a_name()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'create product']
        ])
            ->assignToUser($user)
            ->create();

        $product = factory(Product::class)->raw(['name' => '']);

        $this->post(route('products.store'), $product)
            ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function a_product_requires_a_price()
    {
        $user = $this->loginUser();

        RoleFactory::withPermissions([
            ['name' => 'create product']
        ])
            ->assignToUser($user)
            ->create();

        $product = factory(Product::class)->raw(['price' => '']);

        $this->post(route('products.store'), $product)
            ->assertSessionHasErrors(['price']);
    }
}
