<?php

namespace Tests\Setup;

use App\Comment;
use App\Product;
use App\User;

/**
 * Class ProductFactory
 * @package Tests\Setup
 */
class ProductFactory
{

    /**
     * @var int
     */
    protected int $commentsCount = 0;

    /**
     * @var User
     */
    protected User $user;

    /**
     * @param $count
     * @return $this
     */
    public function withComments($count): self
    {
        $this->commentsCount = $count;

        return $this;
    }

    /**
     * @param $user
     * @return $this
     */
    public function assignToUser($user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Product
     */
    public function create(): Product
    {
        $product = factory(Product::class)->create();

        factory(Comment::class, $this->commentsCount)->create([
            'product_id' => $product->id,
            'user_id' => $this->user ?? factory(\App\User::class)->create()->id
        ]);

        return $product;
    }
}
