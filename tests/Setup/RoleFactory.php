<?php

namespace Tests\Setup;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class RoleFactory
 * @package Tests\Setup
 */
class RoleFactory
{

    /**
     * @var array
     */
    private array $permissions = [];

    /**
     * @var Model|null
     */
    private ?Model $user = null;

    /**
     * @param array $permissions
     * @return $this
     */
    public function withPermissions(array $permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @param $user
     * @return $this
     */
    public function assignToUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param string $roleName
     * @return Model
     */
    public function create(string $roleName = 'user')
    {
        $role = Role::create(['name' => $roleName]);

        if (count($this->permissions)) {
            collect($this->permissions)->mapInto(Permission::class)->each(function ($permission) use ($role) {
                $permission->save();
                if ($this->user) {
                    $this->user->givePermissionTo($permission);
                }
                $role->givePermissionTo($permission);
            });
        }

        return $role;
    }
}
