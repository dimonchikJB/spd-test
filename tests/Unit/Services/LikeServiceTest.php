<?php

namespace Tests\Unit\Services;

use App\Comment;
use App\Like;
use App\Services\LikeService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class LikeServiceTest
 * @package Tests\Unit\Services
 */
class LikeServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var LikeService
     */
    protected LikeService $likeService;

    /**
     * @retun void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->likeService = app(LikeService::class);
    }

    /** @test */
    public function it_can_like_subject()
    {
        $user = $this->loginUser();

        $comment = factory(Comment::class)->create();

        $this->likeService->updateLike($comment, $user, Comment::class, Like::ACTION_LIKE);

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'subject_id' => $comment->id,
            'subject_type' => Comment::class,
            'action' => Like::ACTION_LIKE
        ]);
    }

    /** @test */
    public function it_can_dislike_subject()
    {
        $user = $this->loginUser();

        $comment = factory(Comment::class)->create();

        $this->likeService->updateLike($comment, $user, Comment::class, Like::ACTION_DISLIKE);

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'subject_id' => $comment->id,
            'subject_type' => Comment::class,
            'action' => Like::ACTION_DISLIKE
        ]);
    }
}
