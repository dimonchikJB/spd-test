<?php

namespace Tests\Unit\Services;

use App\Product;
use App\Services\WishlistService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class WishlistServiceTest
 * @package Tests\Unit\Services
 */
class WishlistServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var WishlistService
     */
    protected WishlistService $wishlistService;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->wishlistService = app(WishlistService::class);
    }

    /** @test */
    public function it_can_add_wishlist_item()
    {
        $user = $this->loginUser();

        $product = factory(Product::class)->create();

        $this->wishlistService->addWishlistItem($product->id);

        $this->assertDatabaseHas('wishlists', ['product_id' => $product->id, 'user_id' => $user->id]);
    }
}
