<?php

namespace Tests\Unit\Services;

use App\Product;
use App\Services\ProductService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Setup\ProductFactory;
use Tests\TestCase;

/**
 * Class ProductServiceTest
 * @package Tests\Unit\Services
 */
class ProductServiceTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /**
     * @var ProductService
     */
    protected ProductService $productService;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->productService = app(ProductService::class);
    }

    /** @test */
    public function it_can_add_a_comment()
    {
        $this->loginUser();

        $product = factory(Product::class)->create();

        $data = ['product_id' => $product->id, 'text' => $this->faker->text];

        $this->productService->addComment($data);

        $this->assertDatabaseHas('comments', $data);

    }

    /** @test */
    public function it_can_create_a_product()
    {
        $this->loginUser();

        $product = factory(Product::class)->raw();

        $this->productService->createProduct($product);

        $this->assertDatabaseHas('products', $product);
    }

    /** @test */
    public function it_can_get_comments()
    {
        $user = $this->loginUser();

        $product = ProductFactory::withComments(5)
            ->assignToUSer($user)
            ->create();

        $comments = $this->productService->getComments($product->id);

        $this->assertInstanceOf(\ArrayObject::class, $comments);

        $this->assertCount(5, $comments);
    }

    /** @test */
    public function it_can_update_a_cover_image()
    {
        $product = factory(Product::class)->create();

        $imagePath = 'images/products/test.png';

        $this->assertTrue($this->productService->updateCoverImage($product->id, $imagePath));

        $this->assertDatabaseHas('products', ['cover_image' => $imagePath]);
    }
}
