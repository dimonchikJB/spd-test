<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @return User
     */
    public function loginUser(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        return $user;
    }
}
