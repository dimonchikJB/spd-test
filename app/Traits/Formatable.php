<?php

namespace App\Traits;

use ArrayObject;

/**
 * Trait Formatable
 * @package App\Traits
 */
trait Formatable
{

    /**
     * @return ArrayObject
     */
    public function format(): ArrayObject
    {
        return new ArrayObject($this->toArray(), ArrayObject::ARRAY_AS_PROPS);
    }
}
