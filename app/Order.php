<?php

namespace App;

use App\Traits\Formatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Order
 * @package App
 */
class Order extends Model
{
    use Formatable;

    public const STATUS_PLACED = 'placed';
    public const STATUS_PROCESSING = 'processing';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
      'amount' => 'float'
    ];

    /**
     * @return BelongsTo
     */
    public function cart(): BelongsTo
    {
        return $this->belongsTo(Cart::class);
    }
}
