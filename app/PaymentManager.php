<?php

namespace App;

use App\Services\PayPalService;
use App\Services\StripeService;
use Illuminate\Contracts\Foundation\Application;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 * Class PaymentManager
 * @package App\Payments
 */
class PaymentManager
{
    /**
     * @var Application
     */
    protected Application $app;

    /**
     * @var array
     */
    protected array $payments = [];

    /**
     * PaymentManager constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->payments[$name] ?? $this->resolve($name);
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function resolve($name)
    {

        $config = $this->getConfig('services.' . $name);

        if (is_null($config)) {
            throw new \InvalidArgumentException("Payment service [{$name}] is not defined.");
        }

        $paymentService = 'create' . ucfirst($name) . 'Service';

        if (method_exists($this, $paymentService)) {
            return $this->{$paymentService}($config);
        } else {
            throw new \InvalidArgumentException("Service [{$name}] is not supported.");
        }
    }

    /**
     * @param array $config
     *
     * @return StripeService
     */
    protected function createStripeService(array $config)
    {
        return app(StripeService::class, ['config' => $config]);
    }

    /**
     * @param array $config
     *
     * @return PayPalService
     */
    protected function createPaypalService(array $config)
    {
        $payPalService = app(PayPalService::class, ['config' => $config]);
        $payPalService->setApiContext($this->createPayPalApiContext($config));

        return $payPalService;
    }

    /**
     * @param array $config
     *
     * @return ApiContext
     */
    protected function createPayPalApiContext(array $config): ApiContext
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $config['client_id'],
                $config['secret']
            )
        );
        $apiContext->setConfig($config['settings']);

        return $apiContext;
    }

    /**
     * Get the cache connection configuration.
     *
     * @param string $name
     * @return array
     */
    protected function getConfig($name)
    {
        return $this->app['config']["payment.{$name}"];
    }
}
