<?php

namespace App;

use App\Traits\Formatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Product
 * @package App
 */
class Product extends Model
{
    use Formatable;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'price' => 'float'
    ];

    /**
     * @return string
     */
    public function getImagePath(): string
    {
        return 'public/images/products';
    }

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }
}
