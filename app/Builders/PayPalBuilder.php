<?php

namespace App\Builders;

use ArrayObject;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Common\PayPalModel;
use PayPal\Rest\ApiContext;

/**
 * Class PayPalPaymentBuilder
 * @package App\Payments
 */
class PayPalBuilder
{
    /**
     * @var Payer|null
     */
    protected ?Payer $payer = null;

    /**
     * @var ItemList|null
     */
    protected ?ItemList $itemsList = null;

    /**
     * @var Details|null
     */
    protected ?Details $details = null;

    /**
     * @var Amount|null
     */
    protected ?Amount $amount = null;

    /**
     * @var Transaction|null
     */
    protected ?Transaction $transaction = null;

    /**
     * @var RedirectUrls|null
     */
    protected ?RedirectUrls $redirectUrls = null;

    /**
     * @var Payment|null
     */
    protected ?Payment $payment = null;

    /**
     * @var PaymentExecution|null
     */
    protected ?PaymentExecution $paymentExecution = null;

    /**
     * @param string $paymentMethod
     *
     * @return $this
     */
    public function setPayer(string $paymentMethod): self
    {
        $this->payer = (new Payer())->setPaymentMethod($paymentMethod);

        return $this;
    }

    /**
     * @param int $payerId
     *
     * @return $this
     */
    public function setPaymentExecution(int $payerId): self
    {
        $this->paymentExecution = new PaymentExecution();
        $this->paymentExecution->setPayerId($payerId);
        $this->paymentExecution->addTransaction($this->transaction);

        return $this;
    }

    /**
     * @return PaymentExecution|null
     */
    public function getExecution(): ?PaymentExecution
    {
        return $this->paymentExecution;
    }

    /**
     * @param int $paymentId
     * @param ApiContext $apiContext
     *
     * @return Payment|null
     */
    public function getPayment(int $paymentId, ApiContext $apiContext): ?Payment
    {
        $this->payment = Payment::get($paymentId, $apiContext);

        return$this->payment;
    }

    /**
     * @param ArrayObject $items
     * @param array $fields
     *
     * @return $this
     */
    public function setItems(ArrayObject $items, array $fields): self
    {
        foreach ($items as $item) {
            $PItem = new Item();
            if (isset($item['product'])) {
                foreach ($item['product'] as $field => $value) {
                    if (in_array($field, $fields)) {
                        $method = 'set' . ucfirst($field);
                        $PItem->$method($value);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param array $info
     *
     * @return $this
     */
    public function details(array $info): self
    {
        $this->details = new Details();
        $this->setInfoTo($this->details, $info);

        return $this;
    }

    /**
     * @param string $currency
     * @param float $total
     *
     * @return $this
     */
    public function setAmount(string $currency, float $total): self
    {
        $this->amount = new Amount();
        $this->amount->setCurrency($currency)
            ->setTotal($total)
            ->setDetails($this->details);

        return $this;
    }

    /**
     * @param string $returnUrl
     * @param string $cancelUrl
     *
     * @return $this
     */
    public function setRedirectUrls(string $returnUrl, string $cancelUrl): self
    {
        $this->redirectUrls = new RedirectUrls();
        $this->redirectUrls->setReturnUrl($returnUrl)
            ->setCancelUrl($cancelUrl);

        return $this;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function createTransaction(string $description): self
    {
        $this->transaction = new Transaction();
        $this->transaction->setAmount($this->amount)
            ->setItemList($this->itemsList)
            ->setDescription($description)
            ->setInvoiceNumber(uniqid());

        return $this;
    }

    /**
     * @param string $intent
     *
     * @return Payment
     */
    public function createPayment(string  $intent): Payment
    {
        $this->payment = new Payment();
        $this->payment->setIntent($intent)
            ->setPayer($this->payer)
            ->setRedirectUrls($this->redirectUrls)
            ->setTransactions([$this->transaction]);

        return $this->payment;
    }

    /**
     * @param PayPalModel $subject
     * @param array $info
     */
    protected function setInfoTo(PayPalModel $subject, array $info): void
    {
        foreach ($info as $index => $value) {
            $method = 'set' . ucfirst($index);
            $subject->$method($value);
        }
    }
}
