<?php

namespace App;

use App\Traits\Formatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Cart
 * @package App
 */
class Cart extends Model
{
    use Formatable;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(CartProducts::class);
    }
}
