<?php

namespace App;

use App\Traits\Formatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Like
 * @package App
 */
class Like extends Model
{
    use Formatable;

    public const ACTION_LIKE = 1;
    public const ACTION_DISLIKE = 0;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return MorphTo
     */
    public function subject(): MorphTo
    {
        return $this->morphTo();
    }
}
