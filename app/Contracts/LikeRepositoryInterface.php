<?php

namespace App\Contracts;


use ArrayObject;

/**
 * Class LikeRepository
 * @package App\Repositories
 */
interface LikeRepositoryInterface
{
    /**
     * @param int $subjectId
     * @param int $userId
     * @param string $subjectType
     * @param int $action
     *
     * @return ArrayObject
     */
    public function updateLikeAction(int $subjectId, int $userId, string $subjectType, int $action): ArrayObject;
}
