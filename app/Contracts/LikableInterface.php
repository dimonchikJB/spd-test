<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Interface Likable
 * @package App\Interfaces
 */
interface LikableInterface
{

    /**
     * @return MorphMany
     */
    public function likes(): MorphMany;

    /**
     * @return MorphMany
     */
    public function dislikes(): MorphMany;
}
