<?php

namespace App\Contracts;

use ArrayObject;

/**
 * Class WishlistRepository
 * @package App\Http\Repositories
 */
interface WishlistRepositoryInterface
{
    /**
     * @param int $productId
     *
     * @return ArrayObject
     */
    public function create(int $productId): ArrayObject;
}
