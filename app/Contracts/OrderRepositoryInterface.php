<?php

namespace App\Contracts;

use ArrayObject;

/**
 * Class OrderRepository
 * @package App\Repositories
 */
interface OrderRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function create(array $data): ArrayObject;

    /**
     * @param int $id
     *
     * @return ArrayObject
     */
    public function getOrderById(int $id): ArrayObject;
}
