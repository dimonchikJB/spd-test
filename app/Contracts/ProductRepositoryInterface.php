<?php

namespace App\Contracts;

use ArrayObject;

/**
 * Class ProductRepository
 * @package App\Repositories
 */
interface ProductRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function addComment(array $data): ArrayObject;

    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function create(array $data): ArrayObject;

    /**
     * @param int $productId
     *
     * @return ArrayObject
     */
    public function getComments(int $productId): ArrayObject;

    /**
     * @param int $productId
     * @param array $data
     *
     * @return int
     */
    public function update(int $productId, array $data): int;

    /**
     * @param int $id
     *
     * @return ArrayObject
     */
    public function findById(int $id): ArrayObject;
}
