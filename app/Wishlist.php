<?php

namespace App;

use App\Traits\Formatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Wishlist
 * @package App
 */
class Wishlist extends Model
{
    use Formatable;

    /**
     * @var array
     */
    protected $guarded = [];
}
