<?php

namespace App\Http\Controllers;

use App\Product;
use App\Services\ProductService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\ProductCommentStoreRequest;

/**
 * Class ProductCommentController
 * @package App\Http\Controllers
 */
class ProductCommentController extends Controller
{

    /**
     * @param ProductCommentStoreRequest $request
     * @param ProductService $productService
     *
     * @return RedirectResponse
     */
    public function store(ProductCommentStoreRequest $request, ProductService $productService): RedirectResponse
    {
        $productService->addComment($request->all());

        return redirect()->route('products.index');
    }

    /**
     * @param Product $product
     * @param ProductService $productService
     *
     * @return JsonResponse
     */
    public function index(Product $product, ProductService $productService): JsonResponse
    {
        $comments = $productService->getComments($product->id);

        return response()->json(compact('comments'));
    }
}
