<?php

namespace App\Http\Controllers;

use App\Http\Requests\WishlistStoreRequest;
use App\Services\WishlistService;
use Illuminate\Http\RedirectResponse;

/**
 * Class WishListController
 * @package App\Http\Controllers
 */
class WishListController extends Controller
{

    /**
     * @param WishlistStoreRequest $request
     * @param WishlistService $wishlistService
     * @return RedirectResponse
     */
    public function store(WishlistStoreRequest $request, WishlistService $wishlistService)
    {
        $wishlistService->addWishlistItem($request->input('product_id'));

        return redirect()->route('wishlists.index');
    }
}
