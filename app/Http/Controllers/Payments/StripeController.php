<?php

namespace App\Http\Controllers\Payments;

use App\Facades\Payment;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

/**
 * Class StripePaymentController
 * @package App\Http\Controllers
 */
class StripeController extends Controller
{

    /**
     * @param PaymentRequest $request
     *
     * @return JsonResponse
     */
    public function store(PaymentRequest $request): JsonResponse
    {
        $payment = Payment::get($request->input('payment_code'))
            ->charge($request->input('order_id'), $request->input('payment_method'));

        return response()->json($payment);
    }
}
