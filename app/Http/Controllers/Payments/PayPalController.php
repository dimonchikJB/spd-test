<?php

namespace App\Http\Controllers\Payments;

use App\Facades\Payment;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Services\PayPalService;
use Illuminate\Http\JsonResponse;

/**
 * Class PayPalPaymentController
 * @package App\Http\Controllers
 */
class PayPalController extends Controller
{

    /**
     * @param PaymentRequest $request
     *
     * @return JsonResponse
     */
    public function store(PaymentRequest $request): JsonResponse
    {
        $payment = Payment::get($request->input('payment_code'))
            ->createPayment($request->input('order_id'), $request->input('payment_method'));

        return response()->json($payment);
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        if (request()->success && request()->success === 'true') {
            /** @var PayPalService $payment */
            $payment = Payment::get(request()->payment_code);
            $amount = session('payment_' . request()->paymentId);
            $payment = $payment->executePayment(request()->paymentId, request()->PayerID, $amount);

            return response()->json($payment);
        }

        return response()->json(['message' => 'Something went wrong!']);
    }
}
