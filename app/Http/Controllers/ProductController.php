<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Product;
use App\Services\ProductService;
use Exception;
use Illuminate\Http\RedirectResponse;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @param ProductStoreRequest $request
     * @param ProductService $service
     *
     * @return RedirectResponse
     */
    public function store(ProductStoreRequest $request, ProductService $service): RedirectResponse
    {
        $product = $service->createProduct($request->all());

        return redirect()->route('products.destroy', ['product' => $product->id]);
    }

    /**
     * @param Product $product
     * @throws Exception
     *
     * @return RedirectResponse
     */
    public function destroy(Product $product): RedirectResponse
    {
        $this->authorize('delete product');

        $product->delete();

        return redirect()->route('products.index');
    }
}
