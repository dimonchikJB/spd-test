<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Services\LikeService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Auth\Access\AuthorizationException;

/**
 * Class LikeController
 * @package App\Http\Controllers
 */
class CommentLikeController extends Controller
{
    /**
     * @param Comment $comment
     * @param LikeService $likeService
     * @throws AuthorizationException
     *
     * @return RedirectResponse
     */
    public function update(Comment $comment, LikeService $likeService): RedirectResponse
    {
        $this->authorize('like comment');

        $likeService->updateLike($comment, auth()->user(), Comment::class, request()->action);

        return redirect()->route('products.show', ['product' => $comment->product->id]);
    }

    /**
     * @param Comment $comment
     * @param LikeService $likeService
     * @throws AuthorizationException
     *
     * @return RedirectResponse
     */
    public function destroy(Comment $comment, LikeService $likeService): RedirectResponse
    {
        $this->authorize('dislike comment');

        $likeService->removeLike($comment, auth()->user(), Comment::class);

        return redirect()->route('products.show', ['product' => $comment->product->id]);
    }
}
