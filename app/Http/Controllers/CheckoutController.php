<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Services\OrderService;
use Illuminate\Http\RedirectResponse;

/**
 * Class CheckoutController
 * @package App\Http\Controllers
 */
class CheckoutController extends Controller
{

    /**
     * @param OrderStoreRequest $request
     * @param OrderService $orderService
     *
     * @return RedirectResponse
     */
    public function store(OrderStoreRequest $request, OrderService $orderService): RedirectResponse
    {
        $orderService->createOrder($request->all(), auth()->user());

        return redirect()->home();
    }
}
