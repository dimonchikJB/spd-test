<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoverImageUpdateRequest;
use App\Product;
use App\Services\ProductService;
use Illuminate\Http\RedirectResponse;

/**
 * Class ProductCoverImageController
 * @package App\Http\Controllers
 */
class ProductCoverImageController extends Controller
{
    /**
     * @param CoverImageUpdateRequest $request
     * @param Product $product
     * @param ProductService $productService
     *
     * @return RedirectResponse
     */
    public function update(CoverImageUpdateRequest $request, Product $product, ProductService $productService): RedirectResponse
    {
        $image = $request->file('image');

        $productService->updateCoverImage($product->id, $image->store($product->getImagePath()));

        return redirect()->route('products.show', ['product' => $product->id]);
    }
}
