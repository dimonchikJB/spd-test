<?php

namespace App\Services;

use App\Builders\PayPalBuilder;
use App\Contracts\OrderRepositoryInterface;
use PayPal\Api\Payment;
use PayPal\Rest\ApiContext;

class PayPalService
{
    /**
     * @var array
     */
    protected array $config;

    /**
     * @var PayPalBuilder
     */
    protected PayPalBuilder $payPalPaymentBuilder;

    /**
     * @var ApiContext
     */
    protected ApiContext $apiContext;

    /**
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;

    /**
     * @var OrderService
     */
    protected OrderService $orderService;

    /**
     * @param array $config
     * @param OrderService $orderService
     * @param PayPalBuilder $payPalPaymentBuilder
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        array $config,
        OrderService $orderService,
        PayPalBuilder $payPalPaymentBuilder,
        OrderRepositoryInterface $orderRepository)
    {
        $this->config = $config;
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
        $this->payPalPaymentBuilder = $payPalPaymentBuilder;
    }

    /**
     * @param int $orderId
     * @param string $paymentMethod
     * @param string $description
     * @param string $intent
     *
     * @return string|null
     */
    public function createPayment(int $orderId, string $paymentMethod, string $description = 'sale', string $intent = 'sale'): string
    {
        $order = $this->orderRepository->getOrderById($orderId);
        $items = $this->orderService->getOrderItemsById($orderId);

        $payment = $this->payPalPaymentBuilder
            ->setPayer($paymentMethod)
            ->setItems($items, ['sku', 'name', 'price'])
            ->setAmount($this->config['currency'], $order->amount)
            ->createTransaction($description)
            ->setRedirectUrls(
                route('paypal-redirect') . '?success=true',
                route('paypal-redirect') . '?success=false'
            )
            ->createPayment($intent);
        try {
            $payment->create($this->apiContext);
            session('payment_' . $payment->id, $order->amount);
        } catch (\Exception $e) {
            logger($e->getMessage());
        }

        return $payment;
    }

    /**
     * @param int $paymentId
     * @param int $payerId
     * @param float $amount
     * @param string $description
     *
     * @return Payment
     */
    public function executePayment(int $paymentId, int $payerId, float $amount, string $description = 'sale'): Payment
    {
        $payment = $this->payPalPaymentBuilder->setPaymentExecution($payerId)
            ->setAmount($this->config['currency'], $amount)
            ->createTransaction($description)
            ->getPayment($paymentId, $this->apiContext);

        try {
            $payment->execute($this->payPalPaymentBuilder->getExecution(), $this->apiContext);
        } catch (\Exception $e) {
            logger($e->getMessage());
        }

        return $payment;
    }

    /**
     * @param ApiContext $apiContext
     */
    public function setApiContext(ApiContext $apiContext): void
    {
        $this->apiContext = $apiContext;
    }
}
