<?php

namespace App\Services;

use App\Contracts\ProductRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Class CartService
 * @package App\Services
 */
class CartService
{
    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param Authenticatable $user
     *
     * @return float
     */
    public function getTotalAmount(Authenticatable $user): float
    {
        $cartItems = $user->activeCart->items->pluck('product_id')->map(function ($item) {
            return $this->productRepository->findById((int)$item)->price;
        });

        return $cartItems->reduce(function ($carry, $item) {
           return (float)$carry + (float)$item;
        });
    }
}
