<?php

namespace App\Services;

use App\Contracts\ProductRepositoryInterface;
use ArrayObject;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $repository;

    /**
     * @param ProductRepositoryInterface $repository
     */
    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function addComment(array $data): ArrayObject
    {
        return $this->repository->addComment($data);
    }

    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function createProduct(array $data): ArrayObject
    {
        return $this->repository->create($data);
    }

    /**
     * @param int $productId
     *
     * @return ArrayObject
     */
    public function getComments(int $productId): ArrayObject
    {
        return $this->repository->getComments($productId);
    }

    /**
     * @param int $productId
     * @param string $path
     *
     * @return bool
     */
    public function updateCoverImage(int $productId, string $path): bool
    {
        return (bool)$this->repository->update($productId, ['cover_image' => trim($path, '/')]);
    }
}
