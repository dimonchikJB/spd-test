<?php

namespace App\Services;

use App\Contracts\OrderRepositoryInterface;
use App\Order;
use ArrayObject;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Class OrderService
 * @package App\Services
 */
class OrderService
{
    /**
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;

    /**
     * @var CartService
     */
    protected CartService $cartService;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param CartService $cartService
     */
    public function __construct(OrderRepositoryInterface $orderRepository, CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param array $data
     * @param Authenticatable $user
     *
     * @return ArrayObject
     */
    public function createOrder(array $data, Authenticatable $user): ArrayObject
    {
        $data['amount'] = $this->cartService->getTotalAmount($user);
        $data['status'] = Order::STATUS_PLACED;

        return $this->orderRepository->create($data);
    }

    /**
     * @param int $orderId
     *
     * @return ArrayObject
     */
    public function getOrderItemsById(int $orderId): ArrayObject
    {
        $order = $this->orderRepository->getOrderByIdWithRelations($orderId, ['cart.items.product']);

         return new ArrayObject($order->cart['items']);
    }
}
