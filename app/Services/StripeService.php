<?php

namespace App\Services;

use App\Contracts\OrderRepositoryInterface;
use App\Repositories\UserRepository;

/**
 * Class StripeService
 * @package App\Payments
 */
class StripeService
{

    /**
     * @var array
     */
    protected array $config;

    /**
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;

    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * @param array $config
     * @param OrderRepositoryInterface $orderRepository
     * @param UserRepository $userRepository
     */
    public function __construct(array $config, OrderRepositoryInterface $orderRepository, UserRepository $userRepository)
    {
        $this->config = $config;
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param int $orderId
     * @param string $paymentMethod
     * @param int $userId
     */
    public function charge(int $orderId, string $paymentMethod, int $userId = 0): void
    {
        $user = $this->userRepository->firstOrNew($userId);
        $amount = $this->orderRepository->getOrderById($orderId)->amount;

        try {
            $user->charge($amount, $paymentMethod);
            session('payment', 'Payment was successful!');
        } catch (\Exception $e) {
            logger($e->getMessage());
            session('payment_error', 'Something went wrong!');
        }
    }
}
