<?php

namespace App\Services;

use App\Contracts\WishlistRepositoryInterface;
use ArrayObject;

/**
 * Class WishlistService
 * @package App\Services
 */
class WishlistService
{
    /**
     * @var WishlistRepositoryInterface
     */
    private WishlistRepositoryInterface $repository;

    /**
     * @param WishlistRepositoryInterface $repository
     */
    public function __construct(WishlistRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $productId
     *
     * @return ArrayObject
     */
    public function addWishlistItem(int $productId ): ArrayObject
    {
        return $this->repository->create($productId);
    }
}
