<?php

namespace App\Services;

use App\Contracts\LikeRepositoryInterface;
use Exception;
use App\Contracts\LikableInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LikeService
 * @package App\Services
 */
class LikeService
{
    /**
     * @var LikeRepositoryInterface
     */
    protected LikeRepositoryInterface $likeRepository;

    /**
     * @param LikeRepositoryInterface $likeRepository
     */
    public function __construct(LikeRepositoryInterface $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    /**
     * @param Model $subject
     * @param Authenticatable $user
     * @param string $subjectType
     * @param int $action
     *
     * @return void
     */
    public function updateLike(Model $subject, Authenticatable $user, string $subjectType, int $action): void
    {
        try {
            if (!in_array(LikableInterface::class, class_implements($subjectType))) {
                throw new Exception('The class: '. $subjectType .' must implement: ' . LikableInterface::class);
            }

            $this->likeRepository->updateLikeAction($subject->id, $user->id, $subjectType, $action);
        } catch (Exception $e) {
            logger($e->getMessage());
        }
    }

    /**
     * @param Model $subject
     * @param Authenticatable $user
     * @param string $subjectType
     *
     * @return int
     */
    public function removeLike(Model $subject, Authenticatable $user, string $subjectType): int
    {
        return $this->likeRepository->deleteLike($subject->id, $user->id, $subjectType);
    }
}
