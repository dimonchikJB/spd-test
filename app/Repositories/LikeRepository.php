<?php

namespace App\Repositories;

use App\Contracts\LikeRepositoryInterface;
use App\Like;
use ArrayObject;

/**
 * Class LikeRepository
 * @package App\Repositories
 */
class LikeRepository implements LikeRepositoryInterface
{
    /**
     * @var string
     */
    private string $entity = Like::class;

    /**
     * @param int $subjectId
     * @param int $userId
     * @param string $subjectType
     * @param int $action
     *
     * @return ArrayObject
     */
    public function updateLikeAction(int $subjectId, int $userId, string $subjectType, int $action): ArrayObject
    {
        return $this->entity::updateOrCreate(
            ['subject_id' => $subjectId, 'user_id' => $userId, 'subject_type' => $subjectType],
            ['action' => $action]
        )->format();
    }

    /**
     * @param int $subjectId
     * @param int $userId
     * @param string $subjectType
     *
     * @return int
     */
    public function deleteLike(int $subjectId, int $userId, string $subjectType): int
    {
        return $this->entity::where('subject_id', $subjectId)
            ->where('user_id', $userId)
            ->where('subject_type', $subjectType)
            ->delete();
    }
}
