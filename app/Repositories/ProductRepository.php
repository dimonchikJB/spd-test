<?php declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\ProductRepositoryInterface;
use App\Product;
use ArrayObject;

/**
 * Class ProductRepository
 * @package App\Repositories
 */
class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var string
     */
    private string $entity = Product::class;

    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function addComment(array $data): ArrayObject
    {
        $comment = $this->entity::find($data['product_id'])->comments()->create([
            'user_id' => auth()->user()->id,
            'text' => $data['text']
        ]);

        return $comment->format();
    }

    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function create(array $data): ArrayObject
    {
        $product = $this->entity::create($data);

        return $product->format();
    }

    /**
     * @param int $productId
     *
     * @return ArrayObject
     */
    public function getComments(int $productId): ArrayObject
    {
        $product = $this->entity::with('comments')->find($productId);

        return new ArrayObject($product->comments->toArray());
    }

    /**
     * @param int $productId
     * @param array $data
     *
     * @return int
     */
    public function update(int $productId, array $data): int
    {
        return $this->entity::where('id', $productId)
            ->update($data);
    }

    /**
     * @param int $id
     *
     * @return ArrayObject
     */
    public function findById(int $id): ArrayObject
    {
        return $this->entity::find($id)
            ->format();
    }
}
