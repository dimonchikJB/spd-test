<?php declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\WishlistRepositoryInterface;
use App\Wishlist;
use ArrayObject;

/**
 * Class WishlistRepository
 * @package App\Http\Repositories
 */
class WishlistRepository implements WishlistRepositoryInterface
{
    /**
     * @var string
     */
    private string $entity = Wishlist::class;

    /**
     * @param int $productId
     *
     * @return ArrayObject
     */
    public function create(int $productId): ArrayObject
    {
        $wishlist = $this->entity::create([
            'product_id' => $productId,
            'user_id' => auth()->user()->id
        ]);

        return $wishlist->format();
    }
}
