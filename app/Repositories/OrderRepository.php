<?php

namespace App\Repositories;

use App\Contracts\OrderRepositoryInterface;
use App\Order;
use ArrayObject;

/**
 * Class OrderRepository
 * @package App\Repositories
 */
class OrderRepository implements OrderRepositoryInterface
{
    /**
     * @var string
     */
    protected string $entity = Order::class;

    /**
     * @param array $data
     *
     * @return ArrayObject
     */
    public function create(array $data): ArrayObject
    {
        return $this->entity::create($data)
            ->format();
    }

    /**
     * @param int $id
     *
     * @return ArrayObject
     */
    public function getOrderById(int $id): ArrayObject
    {
        return $this->entity::find($id)->format();
    }

    /**
     * @param int $id
     * @param array $relations
     *
     * @return ArrayObject
     */
    public function getOrderByIdWithRelations(int $id, array $relations): ArrayObject
    {
        return $this->entity::with($relations)->find($id)->format();
    }
}
