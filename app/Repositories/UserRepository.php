<?php

namespace App\Repositories;

use App\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * @var string
     */
    protected string $entity = User::class;

    /**
     * @param int $userId
     * @return User
     */
    public function firstOrNew(int $userId): User
    {
        return $this->entity::firstOrNew(['id' => $userId]);
    }
}
