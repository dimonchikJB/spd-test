<?php

namespace App;

use App\Traits\Formatable;
use App\Contracts\LikableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Class Comment
 * @package App
 */
class Comment extends Model implements LikableInterface
{
    use Formatable;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return MorphMany
     */
    public function likes(): MorphMany
    {
        return $this->morphMany(Like::class, 'subject')
            ->where('action', '=', Like::ACTION_LIKE);
    }

    /**
     * @return MorphMany
     */
    public function dislikes(): MorphMany
    {
        return $this->morphMany(Like::class, 'subject')
            ->where('action', '=', Like::ACTION_DISLIKE);
    }
}
