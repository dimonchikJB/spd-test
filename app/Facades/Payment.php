<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Payment
 * @package App\Facades
 */
class Payment extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'payment';
    }
}
