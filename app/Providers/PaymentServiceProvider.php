<?php

namespace App\Providers;

use App\PaymentManager;
use Illuminate\Support\ServiceProvider;

/**
 * Class PaymentServiceProvider
 * @package App\Providers
 */
class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('payment', function ($app) {
            return new PaymentManager($app);
        });
    }
}
