<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cart;
use App\Order;
use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'status' => Order::STATUS_PLACED,
        'name' => $faker->name,
        'phone' => '+380990765432',
        'email' => $faker->email,
        'coupon_code' => '',
        'amount' => $faker->randomFloat(2),
        'street' => $faker->streetAddress,
        'cart_id' => factory(Cart::class),
        'country' => $faker->country
    ];
});
