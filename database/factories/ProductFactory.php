<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'sku' => $faker->unique()->randomNumber(),
        'visible' => $faker->boolean,
        'name' => $faker->word,
        'price' => $faker->randomFloat(2),
        'cover_image' => 'public/images/products/jPLLie86khUdMHJDTBvmIRwOJRVcgzRErpYolLR0.png'
    ];
});
